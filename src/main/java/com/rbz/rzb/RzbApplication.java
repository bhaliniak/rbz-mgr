package com.rbz.rzb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RzbApplication {

	public static void main(String[] args) {
		SpringApplication.run(RzbApplication.class, args);
	}
}
