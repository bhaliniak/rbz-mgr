package com.rbz.rzb.user;

import com.rbz.rzb.admin.panel.configuration.models.Performance;
import com.rbz.rzb.admin.panel.configuration.models.PerformanceProxy;
import com.rbz.rzb.admin.panel.configuration.models.Theatre;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dd on 2017-12-10.
 */
@Controller
public class UserPlayController {

    @RequestMapping(value = "/userplay/{someID}", method = RequestMethod.GET)
    public String getAttr(@PathVariable(value="someID") String id, Model theatre, Model playList, Model performanceProxy, Model performanceList){
        /*Theatre selectedTheatre = theatreRepository.findOne(Long.parseLong(id));
        performanceProxy.addAttribute("performanceProxy", new PerformanceProxy());
        playList.addAttribute("playsList", playRepository.findAll());
        theatre.addAttribute("theatre", selectedTheatre);
        List<Performance> allPerformences = (List<Performance>) performanceRepository.findAll();
        List<Performance> thisTheatrePerformance = allPerformences.stream().filter(x->x.getTheatre().getId()==Long.parseLong(id)).collect(Collectors.toList());
        performanceList.addAttribute("performanceList", thisTheatrePerformance);
        */return "/chosenuserplay";
    }
}
