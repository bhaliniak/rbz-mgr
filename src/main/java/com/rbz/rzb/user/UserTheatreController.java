package com.rbz.rzb.user;

import com.rbz.rzb.admin.panel.configuration.ChangeSeatStatusProxy;
import com.rbz.rzb.admin.panel.configuration.PerformanceCreator;
import com.rbz.rzb.admin.panel.configuration.ReservationProxy;
import com.rbz.rzb.admin.panel.configuration.models.Performance;
import com.rbz.rzb.admin.panel.configuration.models.PerformanceProxy;
import com.rbz.rzb.admin.panel.configuration.models.Theatre;
import com.rbz.rzb.admin.panel.configuration.repositories.PerformanceRepository;
import com.rbz.rzb.admin.panel.configuration.repositories.PlayRepository;
import com.rbz.rzb.admin.panel.configuration.repositories.TheatreRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.persistence.Entity;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dd on 2017-12-10.
 */
@Controller
public class UserTheatreController{

    private TheatreRepository theatreRepository;

    private PlayRepository playRepository;

    private PerformanceRepository performanceRepository;

    private PerformanceCreator performanceCreator;

    public UserTheatreController(TheatreRepository theatreRepository, PlayRepository playRepository, PerformanceRepository performanceRepository, PerformanceCreator performanceCreator){
        this.theatreRepository = theatreRepository;
        this.playRepository = playRepository;
        this.performanceCreator = performanceCreator;
        this.performanceRepository = performanceRepository;
    }

    @RequestMapping(value = "/user/userTheatre", method = RequestMethod.GET)
    public String selectTheatre(Model thaatersList){
        thaatersList.addAttribute("theatresList", theatreRepository.findAll());
        return "/usertheatre";
    }

    @RequestMapping(value = "/user/userChooseTheatre", method = RequestMethod.POST)
    public String choose(@ModelAttribute Theatre theatre){
        return "redirect:/user/userTheatre/"+theatre.getId();
    }

    @RequestMapping(value = "/user/userTheatre/{someID}", method = RequestMethod.GET)
    public String getAttr(@PathVariable(value="someID") String id, Model theatre, Model playList, Model performanceProxy, Model performanceList){
        Theatre selectedTheatre = theatreRepository.findOne(Long.parseLong(id));
        performanceProxy.addAttribute("performanceProxy", new PerformanceProxy());
        playList.addAttribute("playsList", playRepository.findAll());
        theatre.addAttribute("theatre", selectedTheatre);
        List<Performance> allPerformences = (List<Performance>) performanceRepository.findAll();
        List<Performance> thisTheatrePerformance = allPerformences.stream().filter(x->x.getTheatre().getId()==Long.parseLong(id)).collect(Collectors.toList());
        performanceList.addAttribute("performanceList", thisTheatrePerformance);
        return "/userchosentheatre";
    }

    @RequestMapping(value = "/user/userChoosePerformance", method = RequestMethod.POST)
    public String choosePerformance(@ModelAttribute Performance performance){
        return "redirect:/user/userPerformance/"+performance.getId();
    }

    @RequestMapping(value = "/user/userPerformance/{someID}", method = RequestMethod.GET)
    public String getSecondAttr(@PathVariable(value="someID") String id, Model performanceModel, Model seats, Model seatnumber, Model reservation){
        Performance performance = this.performanceRepository.findOne(Long.parseLong(id));
        seatnumber.addAttribute("seatNumber", performance.getTheatre().getSeatNumbers());
        performanceModel.addAttribute("performance", performance);
        seats.addAttribute("seats", this.performanceCreator.getReservationSeatsToDisplay(performance));
        reservation.addAttribute("reservation", new ReservationProxy());
        return "/userchosenperformance";
    }

    @RequestMapping(value = "/user/userChangeSeatStatus", method = RequestMethod.POST)
    public String changeStatus(@ModelAttribute ReservationProxy reservationProxy){
        this.performanceCreator.changeSeatStatusbyUser(reservationProxy);
        return "redirect:/user/userPerformance/" + reservationProxy.getReservationPerformanceId();
    }



}
