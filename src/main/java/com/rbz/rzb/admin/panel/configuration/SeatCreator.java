package com.rbz.rzb.admin.panel.configuration;

import com.rbz.rzb.admin.panel.configuration.models.Seat;
import com.rbz.rzb.admin.panel.configuration.models.Theatre;
import com.rbz.rzb.admin.panel.configuration.repositories.SeatRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dd on 2017-12-09.
 */
@Service
public class SeatCreator {

    private SeatRepository seatRepository;

    public SeatCreator(SeatRepository seatRepository){
        this.seatRepository = seatRepository;
    }

    public List<Seat> createSeatsForTheater(Theatre theatre){
        List<Seat> seats = new ArrayList<Seat>();
        for(int i=1;i<theatre.getRowNumbers()+1;i++){
            for(int j=1;j<theatre.getSeatNumbers()+1;j++){
                seats.add(new Seat(i, j, theatre));
            }
        }
        this.seatRepository.save(seats);
        return seats;
    }

}
