package com.rbz.rzb.admin.panel.configuration.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dd on 2017-12-09.
 */
@Entity
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String surname;

    private String number;

    private String email;

    @OneToMany(mappedBy = "reservation", cascade = CascadeType.ALL)
    private List<ReservationSeat> reservationSeats;

    public Reservation(){

    }

    public Reservation(String name, String surname, String number, String email){
        this.name = name;
        this.surname = surname;
        this.number = number;
        this.email = email;
        this.reservationSeats = new ArrayList<ReservationSeat>();
    }

    public void addReservationSeat(ReservationSeat reservationSeat){
        this.reservationSeats.add(reservationSeat);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<ReservationSeat> getReservationSeats() {
        return reservationSeats;
    }

    public void setReservationSeats(List<ReservationSeat> reservationSeats) {
        this.reservationSeats = reservationSeats;
    }
}
