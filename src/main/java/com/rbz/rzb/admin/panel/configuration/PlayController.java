package com.rbz.rzb.admin.panel.configuration;

import com.rbz.rzb.admin.panel.configuration.models.Performance;
import com.rbz.rzb.admin.panel.configuration.models.PerformanceProxy;
import com.rbz.rzb.admin.panel.configuration.models.Play;
import com.rbz.rzb.admin.panel.configuration.models.Theatre;
import com.rbz.rzb.admin.panel.configuration.repositories.PerformanceRepository;
import com.rbz.rzb.admin.panel.configuration.repositories.PlayRepository;
import com.rbz.rzb.admin.panel.configuration.repositories.TheatreRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dd on 2017-12-09.
 */
@Controller
public class PlayController {

    private PlayRepository playRepository;

    private TheatreRepository theatreRepository;

    private PerformanceRepository performanceRepository;

    public PlayController(TheatreRepository theatreRepository, PlayRepository playRepository, PerformanceRepository performanceRepository){
        this.playRepository = playRepository;
        this.performanceRepository = performanceRepository;
        this.theatreRepository = theatreRepository;
    }

    @RequestMapping(value = "/play", method = RequestMethod.GET)
    public String addTheatre(Model addSingleModel, Model playsList){
        addSingleModel.addAttribute("play", new Play());
        playsList.addAttribute("playsList", playRepository.findAll());
        return "/play";
    }

    @RequestMapping(value = "/play", method = RequestMethod.POST)
    public String addplay(@ModelAttribute Play play){
        playRepository.save(play);
        return "redirect:/play";
    }

    @RequestMapping(value = "/choosePlay", method = RequestMethod.POST)
    public String choose(@ModelAttribute Play play){
        return "redirect:/play/"+play.getId();
    }

    @RequestMapping(value = "/play/{someID}", method = RequestMethod.GET)
    public String getAttr(@PathVariable(value="someID") String id, Model play, Model theatresList, Model performanceProxy, Model performanceList){
        Play selectedPlay = playRepository.findOne(Long.parseLong(id));
        performanceProxy.addAttribute("performanceProxy", new PerformanceProxy());
        theatresList.addAttribute("theatresList", theatreRepository.findAll());
        play.addAttribute("play", selectedPlay);
        List<Performance> allPerformences = (List<Performance>) performanceRepository.findAll();
        List<Performance> thisPlayPerformance = allPerformences.stream().filter(x->x.getPlay().getId()==Long.parseLong(id)).collect(Collectors.toList());
        performanceList.addAttribute("performanceList", thisPlayPerformance);
        return "/chosenplay";
    }

}
