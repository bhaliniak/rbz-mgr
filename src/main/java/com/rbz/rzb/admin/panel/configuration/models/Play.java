package com.rbz.rzb.admin.panel.configuration.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by dd on 2017-12-08.
 */
@Entity
public class Play {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String description;

    @OneToMany(mappedBy = "play", cascade = CascadeType.ALL)
    private List<Performance> performance;

    public void addperformance(Performance performance){
        this.performance.add(performance);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Performance> getPerformance() {
        return performance;
    }

    public void setPerformance(List<Performance> performance) {
        this.performance = performance;
    }
}
