package com.rbz.rzb.admin.panel.configuration.repositories;

import com.rbz.rzb.admin.panel.configuration.models.Theatre;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by dd on 2017-12-03.
 */
@Repository
public interface TheatreRepository extends CrudRepository<Theatre, Long>{
}
