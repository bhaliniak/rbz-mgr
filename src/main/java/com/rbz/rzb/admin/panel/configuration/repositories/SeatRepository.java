package com.rbz.rzb.admin.panel.configuration.repositories;

import com.rbz.rzb.admin.panel.configuration.models.Seat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by dd on 2017-12-09.
 */
@Repository
public interface SeatRepository extends CrudRepository<Seat, Long>{
}
