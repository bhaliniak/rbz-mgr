package com.rbz.rzb.admin.panel.configuration.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dd on 2017-12-09.
 */
@Entity
public class Seat {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private int rowNumber;

    private int seatNumber;

    @ManyToOne
    private Theatre theatre;

    @OneToMany(mappedBy = "seat", cascade = CascadeType.ALL)
    private List<ReservationSeat> reservationSeat;

    public Seat(){

    }

    public Seat(int rowNumber, int seatNumber, Theatre theatre){
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.theatre = theatre;
        this.reservationSeat = new ArrayList<ReservationSeat>();
    }

    public void addReservationSeat(ReservationSeat reservationSeat){
        this.reservationSeat.add(reservationSeat);
    }

    public List<ReservationSeat> getReservationSeat() {
        return reservationSeat;
    }

    public void setReservationSeat(List<ReservationSeat> reservationSeat) {
        this.reservationSeat = reservationSeat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public Theatre getTheatre() {
        return theatre;
    }

    public void setTheatre(Theatre theatre) {
        this.theatre = theatre;
    }
}
