package com.rbz.rzb.admin.panel;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by dd on 2017-12-06.
 */
@Controller
public class AdminPanelController {

    @RequestMapping(value = "adminPanel", method = RequestMethod.GET)
    public String adminPanelGet(Model model){
        return "adminPanel";
    }

}
