package com.rbz.rzb.admin.panel.configuration.models;

/**
 * Created by dd on 2017-12-03.
 */

import javax.persistence.*;
import java.util.List;

@Entity
public class Theatre {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String street;

    private String streetNumber;

    private String city;

    private String email;

    private String phoneNumber;

    private int rowNumbers;

    private int seatNumbers;

    private int allSeatsNumber;

    @OneToMany(mappedBy = "theatre", cascade = CascadeType.ALL)
    private List<Performance> performance;

    @OneToMany(mappedBy = "theatre", cascade = CascadeType.ALL)
    private List<Seat> seats;

    public void calculateSeatNumber(){
        this.allSeatsNumber = this.rowNumbers * this.seatNumbers;
    }

    public void addPerformance(Performance performance){
        this.performance.add(performance);
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Performance> getPerformance() {
        return performance;
    }

    public void setPerformance(List<Performance> performance) {
        this.performance = performance;
    }

    public int getRowNumbers() {
        return rowNumbers;
    }

    public void setRowNumbers(int rowNumbers) {
        this.rowNumbers = rowNumbers;
    }

    public int getSeatNumbers() {
        return seatNumbers;
    }

    public void setSeatNumbers(int seatNumbers) {
        this.seatNumbers = seatNumbers;
    }

    public int getAllSeatsNumber() {
        return allSeatsNumber;
    }

    public void setAllSeatsNumber(int allSeatsNumber) {
        this.allSeatsNumber = allSeatsNumber;
    }
}
