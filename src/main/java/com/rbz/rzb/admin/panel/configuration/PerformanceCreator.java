package com.rbz.rzb.admin.panel.configuration;

import com.rbz.rzb.admin.panel.configuration.models.*;
import com.rbz.rzb.admin.panel.configuration.repositories.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dd on 2017-12-09.
 */
@Service
public class PerformanceCreator {

    private PlayRepository playRepository;

    private TheatreRepository theatreRepository;

    private PerformanceRepository performanceRepository;

    private ReservationSeatRepository reservationSeatRepository;

    private ReservationRepository reservationRepository;

    public PerformanceCreator(PlayRepository playRepository, TheatreRepository theatreRepository,
                              ReservationSeatRepository reservationSeatRepository, PerformanceRepository performanceRepository,
                              ReservationRepository reservationRepository){
        this.playRepository = playRepository;
        this.reservationRepository = reservationRepository;
        this.reservationSeatRepository = reservationSeatRepository;
        this.theatreRepository = theatreRepository;
        this.performanceRepository = performanceRepository;
    }

    public void savePerformance(PerformanceProxy performanceProxy){
        Theatre theatre = this.theatreRepository.findOne(performanceProxy.getTheatreId());
        Play play = this.playRepository.findOne(performanceProxy.getPlayId());
        Performance performance = new Performance(performanceProxy.getDate(), performanceProxy.getTime(), play, theatre);
        theatre.addPerformance(performance);
        List<ReservationSeat> reservationSeats = createReservationSeat(theatre.getSeats(), performance);
        play.addperformance(performance);
        performance.setReservationSeat(reservationSeats);
        this.performanceRepository.save(performance);
    }

    private List<ReservationSeat> createReservationSeat(List<Seat> seats, Performance performance){
        List<ReservationSeat> reservationSeats = new ArrayList<ReservationSeat>();
        seats.forEach(x ->{
            ReservationSeat reservationSeat = new ReservationSeat(performance, x);
            x.addReservationSeat(reservationSeat);
            reservationSeats.add(reservationSeat);
        });
        this.reservationSeatRepository.save(reservationSeats);
        return reservationSeats;
    }

    public List<ReservationSeatRowProxy> getReservationSeatsToDisplay(Performance performance){
        List<ReservationSeat> reservationSeats = performance.getReservationSeat();
        List<ReservationSeatRowProxy> sortedList = createClearListOfLists(performance.getTheatre().getSeatNumbers(),
                performance.getTheatre().getRowNumbers());
        for(ReservationSeat seat: reservationSeats){
            sortedList.get(seat.getSeat().getRowNumber()-1).getReservationSeats().set(seat.getSeat().getSeatNumber()-1, seat);
        }
        return sortedList;
    }

    private List<ReservationSeatRowProxy> createClearListOfLists(int seatsInRow, int rowsNumber){
        List<ReservationSeatRowProxy> sortedList =  new ArrayList<ReservationSeatRowProxy>();
        for(int i=0;i<rowsNumber;i++){
            ReservationSeatRowProxy reservationSeatRowProxy = new ReservationSeatRowProxy();
            for(int j=0;j<seatsInRow;j++){
                reservationSeatRowProxy.addReservationSeat(new ReservationSeat());
            }
            sortedList.add(reservationSeatRowProxy);
        }
        return sortedList;
    }

    public void changeStatus(String seatId){
        ReservationSeat reservationSeat = this.reservationSeatRepository.findOne(Long.parseLong(seatId));
        if(reservationSeat.getStatus()){
            reservationSeat.setStatus(false);
        }
        else{
            reservationSeat.setStatus(true);
        }
        this.reservationSeatRepository.save(reservationSeat);
    }

    public void changeSeatStatusbyUser(ReservationProxy reservationProxy) {
        ReservationSeat reservationSeat = this.reservationSeatRepository.findOne(Long.parseLong(reservationProxy.getReservationSeatId()));
        if (reservationSeat.getStatus()) {
            reservationSeat.setStatus(false);
        } else {
            reservationSeat.setStatus(true);
        }
        Reservation reservation = this.reservationRepository.findOneByEmail(reservationProxy.getEmail());
        if (reservation != null && isReservationExists(reservationProxy, reservation)) {
            reservation.addReservationSeat(reservationSeat);
            reservationSeat.setReservation(reservation);
        } else {
            reservation = new Reservation(reservationProxy.getName(), reservationProxy.getSurname(),
                    reservationProxy.getNumber(), reservationProxy.getEmail());
            reservation.addReservationSeat(reservationSeat);
            reservationSeat.setReservation(reservation);
        }
        this.reservationRepository.save(reservation);
    }

    private boolean isReservationExists(ReservationProxy reservationProxy, Reservation reservation) {
        return reservationProxy.getName().equals(reservation.getName())
                && reservationProxy.getEmail().equals(reservation.getEmail())
                && reservationProxy.getSurname().equals(reservation.getSurname())
                && reservationProxy.getNumber().equals(reservation.getNumber());
    }



}
