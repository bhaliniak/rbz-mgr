package com.rbz.rzb.admin.panel.configuration.models;

import javax.persistence.*;

/**
 * Created by dd on 2017-12-09.
 */
@Entity
public class ReservationSeat {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private Boolean status;

    @ManyToOne
    private Seat seat;

    @ManyToOne
    private Performance performance;

    @ManyToOne
    private Reservation reservation;

    public ReservationSeat(){

    }

    public ReservationSeat(Performance performance, Seat seat){
        this.performance = performance;
        this.seat = seat;
        this.status = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public Performance getPerformance() {
        return performance;
    }

    public void setPerformance(Performance performance) {
        this.performance = performance;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }
}
