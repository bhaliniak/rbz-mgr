package com.rbz.rzb.admin.panel.configuration.repositories;

import com.rbz.rzb.admin.panel.configuration.models.ReservationSeat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by dd on 2017-12-09.
 */
@Repository
public interface ReservationSeatRepository extends CrudRepository<ReservationSeat, Long>{
}
