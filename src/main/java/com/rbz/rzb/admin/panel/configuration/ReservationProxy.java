package com.rbz.rzb.admin.panel.configuration;

/**
 * Created by dd on 2017-12-12.
 */
public class ReservationProxy {

    private String name;

    private String surname;

    private String number;

    private String email;

    private String reservationSeatId;

    private String reservationPerformanceId;

    public String getReservationPerformanceId() {
        return reservationPerformanceId;
    }

    public void setReservationPerformanceId(String reservationPerformanceId) {
        this.reservationPerformanceId = reservationPerformanceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReservationSeatId() {
        return reservationSeatId;
    }

    public void setReservationSeatId(String reservationSeatId) {
        this.reservationSeatId = reservationSeatId;
    }
}
