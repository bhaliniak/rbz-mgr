package com.rbz.rzb.admin.panel.configuration.repositories;

import com.rbz.rzb.admin.panel.configuration.models.Reservation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;

/**
 * Created by dd on 2017-12-12.
 */
@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long>{
    Reservation findOneByEmail(String email);
}
