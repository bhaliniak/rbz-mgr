package com.rbz.rzb.admin.panel.configuration.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by dd on 2017-12-08.
 */
@Entity
public class Performance {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String date;

    private String time;

    @ManyToOne
    private Play play;

    @ManyToOne
    private Theatre theatre;

    @OneToMany(mappedBy = "performance", cascade = CascadeType.ALL)
    private List<ReservationSeat> reservationSeat;

    public Performance(){

    }

    public Performance(String date, String time, Play play, Theatre theatre){
        this.date = date;
        this.time = time;
        this.play = play;
        this.theatre = theatre;
    }

    public List<ReservationSeat> getReservationSeat() {
        return reservationSeat;
    }

    public void setReservationSeat(List<ReservationSeat> reservationSeat) {
        this.reservationSeat = reservationSeat;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Play getPlay() {
        return play;
    }

    public void setPlay(Play play) {
        this.play = play;
    }

    public Theatre getTheatre() {
        return theatre;
    }

    public void setTheatre(Theatre theatre) {
        this.theatre = theatre;
    }
}
