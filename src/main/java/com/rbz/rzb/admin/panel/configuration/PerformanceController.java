package com.rbz.rzb.admin.panel.configuration;

import com.rbz.rzb.admin.panel.configuration.models.Performance;
import com.rbz.rzb.admin.panel.configuration.models.PerformanceProxy;
import com.rbz.rzb.admin.panel.configuration.models.Play;
import com.rbz.rzb.admin.panel.configuration.repositories.PerformanceRepository;
import com.rbz.rzb.admin.panel.configuration.repositories.PlayRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dd on 2017-12-09.
 */
@Controller
public class PerformanceController {

    private PerformanceCreator performanceCreator;

    private PerformanceRepository performanceRepository;

    public PerformanceController(PerformanceCreator performanceCreator, PerformanceRepository performanceRepository){
        this.performanceCreator = performanceCreator;
        this.performanceRepository = performanceRepository;
    }

    @RequestMapping(value = "/addTheaterPerformance", method = RequestMethod.POST)
    public String addPerformanceFromTheatre(@ModelAttribute PerformanceProxy performanceProxy){
        this.performanceCreator.savePerformance(performanceProxy);
        return "redirect:/theatre/"+performanceProxy.getTheatreId();
    }

    @RequestMapping(value = "/addPlayPerformance", method = RequestMethod.POST)
    public String addPerformanceFromPlay(@ModelAttribute PerformanceProxy performanceProxy){
        this.performanceCreator.savePerformance(performanceProxy);
        return "redirect:/play/"+performanceProxy.getPlayId();
    }

    @RequestMapping(value = "/choosePerformance", method = RequestMethod.POST)
    public String choosePerformance(@ModelAttribute Performance performance){
        return "redirect:/performance/"+performance.getId();
    }

    @RequestMapping(value = "/performance/{someID}", method = RequestMethod.GET)
    public String getAttr(@PathVariable(value="someID") String id, Model performanceModel, Model seats, Model seatnumber){
        Performance performance = this.performanceRepository.findOne(Long.parseLong(id));
        seatnumber.addAttribute("seatNumber", performance.getTheatre().getSeatNumbers());
        performanceModel.addAttribute("performance", performance);
        seats.addAttribute("seats", this.performanceCreator.getReservationSeatsToDisplay(performance));
        return "/chosenperformance";
    }

    @RequestMapping(value = "/changeSeatStatus", method = RequestMethod.POST)
    public String changeStatus(@ModelAttribute ChangeSeatStatusProxy changeSeatStatusProxy){
        this.performanceCreator.changeStatus(changeSeatStatusProxy.getSeatId());
        return "redirect:/performance/"+changeSeatStatusProxy.getPerformanceId();
    }
}
