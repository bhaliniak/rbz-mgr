package com.rbz.rzb.admin.panel.configuration;

import com.rbz.rzb.admin.panel.configuration.models.Performance;
import com.rbz.rzb.admin.panel.configuration.models.PerformanceProxy;
import com.rbz.rzb.admin.panel.configuration.models.Seat;
import com.rbz.rzb.admin.panel.configuration.models.Theatre;
import com.rbz.rzb.admin.panel.configuration.repositories.PerformanceRepository;
import com.rbz.rzb.admin.panel.configuration.repositories.PlayRepository;
import com.rbz.rzb.admin.panel.configuration.repositories.TheatreRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dd on 2017-12-03.
 */
@Controller
public class AddTheatreController {

    private TheatreRepository theatreRepository;

    private PlayRepository playRepository;

    private PerformanceRepository performanceRepository;

    private SeatCreator seatCreator;

    public AddTheatreController(TheatreRepository theatreRepository, PlayRepository playRepository, SeatCreator seatCreator, PerformanceRepository performanceRepository){
        this.performanceRepository = performanceRepository;
        this.theatreRepository = theatreRepository;
        this.playRepository = playRepository;
        this.seatCreator = seatCreator;
    }

    @RequestMapping(value = "/theatre", method = RequestMethod.GET)
    public String addTheatre(Model addSingleModel, Model thaatersList){
        addSingleModel.addAttribute("theatre", new Theatre());
        thaatersList.addAttribute("theatersList", theatreRepository.findAll());
        return "/theatre";
    }

    @RequestMapping(value = "/theatre", method = RequestMethod.POST)
    public String addTheatre(@ModelAttribute Theatre theatre){
        theatre.calculateSeatNumber();
        theatreRepository.save(theatre);
        List<Seat> seats = this.seatCreator.createSeatsForTheater(theatre);
        theatre.setSeats(seats);
        theatreRepository.save(theatre);
        return "redirect:/theatre";
    }

    @RequestMapping(value = "/chooseTheatre", method = RequestMethod.POST)
    public String choose(@ModelAttribute Theatre theatre){
        return "redirect:/theatre/"+theatre.getId();
    }

    @RequestMapping(value = "/theatre/{someID}", method = RequestMethod.GET)
    public String getAttr(@PathVariable(value="someID") String id, Model theatre, Model playList, Model performanceProxy, Model performanceList){
        Theatre selectedTheatre = theatreRepository.findOne(Long.parseLong(id));
        performanceProxy.addAttribute("performanceProxy", new PerformanceProxy());
        playList.addAttribute("playsList", playRepository.findAll());
        theatre.addAttribute("theatre", selectedTheatre);
        List<Performance> allPerformences = (List<Performance>) performanceRepository.findAll();
        List<Performance> thisTheatrePerformance = allPerformences.stream().filter(x->x.getTheatre().getId()==Long.parseLong(id)).collect(Collectors.toList());
        performanceList.addAttribute("performanceList", thisTheatrePerformance);
        return "/chosentheatre";
    }

}
