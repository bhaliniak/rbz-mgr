package com.rbz.rzb.admin.panel.configuration;

import com.rbz.rzb.admin.panel.configuration.models.Reservation;
import com.rbz.rzb.admin.panel.configuration.models.ReservationSeat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dd on 2017-12-09.
 */
public class ReservationSeatRowProxy {

    private List<ReservationSeat> reservationSeats;

    public ReservationSeatRowProxy(){
        this.reservationSeats = new ArrayList<ReservationSeat>();
    }

    public void addReservationSeat(ReservationSeat reservationSeat){
        this.reservationSeats.add(reservationSeat);
    }

    public List<ReservationSeat> getReservationSeats() {
        return reservationSeats;
    }

    public void setReservationSeats(List<ReservationSeat> reservationSeats) {
        this.reservationSeats = reservationSeats;
    }
}
