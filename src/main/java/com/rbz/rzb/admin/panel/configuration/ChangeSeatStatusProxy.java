package com.rbz.rzb.admin.panel.configuration;

/**
 * Created by dd on 2017-12-10.
 */
public class ChangeSeatStatusProxy {

    private String performanceId;

    private String seatId;

    public String getPerformanceId() {
        return performanceId;
    }

    public void setPerformanceId(String performanceId) {
        this.performanceId = performanceId;
    }

    public String getSeatId() {
        return seatId;
    }

    public void setSeatId(String seatId) {
        this.seatId = seatId;
    }
}
