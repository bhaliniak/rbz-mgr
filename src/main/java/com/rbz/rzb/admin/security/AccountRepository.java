package com.rbz.rzb.admin.security;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dd on 2017-12-06.
 */

public interface AccountRepository extends JpaRepository<Account, String>{

}