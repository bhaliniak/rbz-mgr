package com.rbz.rzb.admin.security;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by dd on 2017-12-06.
 */
@Entity
public class Account implements Serializable {

    @Id
    private String username;

    private String password;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserRoles> roles;

    public List<UserRoles> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRoles> roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
