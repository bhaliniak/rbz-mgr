package com.rbz.rzb.admin.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import javax.management.relation.*;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by dd on 2017-12-06.
 */
public class MyUserDetails extends User {

    String username;
    String password;
    List<UserRoles> roles;

    public MyUserDetails(Account account){
        super(account.getUsername(), account.getPassword(), transformToRoles(account.getRoles()));
        this.password = account.getPassword();
        this.username = account.getUsername();
        this.roles = account.getRoles();
    }

    public String toString(){
        return "";
    }

    private static Collection<? extends GrantedAuthority> transformToRoles(List<UserRoles> roles) {

        return roles.stream()
                .map(auth -> new SimpleGrantedAuthority(auth.getRole().name()))
                .collect(Collectors.toList());
    }
}
